﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using TiledSharp;

namespace TiledConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            //string path = args[0];
            ConvertTileSets();

            ConvertTileMaps();
        }

        static void ConvertTileMaps()
        {
            string[] files = Directory.GetFiles("C:/Users/dpete/Documents/programing/E0I/RelicEngine/RelicEngine/Grimphobia/Maps", "*.tmx");

            foreach (string item in files)
            {

                //var a = XDocument.Load(item);
                //var sheet = new TiledSharp.TmxTileset(a.Document, "path");

                var map = new TiledSharp.TmxMap(item);

                string newpath = item.Replace("tmx", "rmf");

                StreamWriter writer = new StreamWriter(newpath);
                writer.WriteLine("rmf-1.0.0");
                writer.WriteLine("PLACEHOLDER_NAME");
                writer.WriteLine(map.Width + "," + map.Height);
                writer.WriteLine(map.TileWidth + "," + map.TileHeight);

                writer.WriteLine("/SC-" + map.Tilesets.Count);
                foreach (var tileset in map.Tilesets)
                {
                    writer.WriteLine("/S-" + tileset.Name + "," + tileset.FirstGid);
                }


                writer.WriteLine("/LC-" + map.Layers.Count);

                foreach (var layer in map.Layers)
                {
                    writer.WriteLine("/L-" + layer.Name);

                    for (int x = 0; x < map.Height; x++)
                    {
                        writer.Write(layer.Tiles[x * map.Height].Gid);
                        for (int y = 1; y < map.Width; y++)
                        {
                            writer.Write("," + layer.Tiles[(x * map.Width) + y].Gid);

                        }
                        writer.WriteLine();
                    }
                }
                int objCount = 0;
                foreach (var objectGroup in map.ObjectGroups)
                {
                    objCount += objectGroup.Objects.Count;
                }

                writer.WriteLine("/EC-" + objCount);
                foreach (var group in map.ObjectGroups)
                {
                    writer.WriteLine("/E-" + group.Name + "," + group.Objects.Count);

                    foreach (var tmxObject in group.Objects)
                    {
                        writer.Write(tmxObject.Id + "-" + (int)tmxObject.X + ":" + (int)tmxObject.Y + "-" + tmxObject.Name + ":" + tmxObject.Type);
                        if(tmxObject.Width > 0 || tmxObject.Height > 0)
                        {
                            writer.Write(";" + "width:" + tmxObject.Width);
                            writer.Write(";" + "height:" + tmxObject.Height);
                        }
                        foreach (var valuePair in tmxObject.Properties)
                        {
                            string value = valuePair.Value;
                            if(valuePair.Value[0] == '#')
                            {

                                value = value.Remove(0,1);
                                value = "0x" + value;
                            }
                            writer.Write(";" + valuePair.Key + ":" + value);
                        }
                        writer.WriteLine();
                    }
                }

                writer.Close();

            }
        }


        static void ConvertTileSets()
        {
            string[] files = Directory.GetFiles("C:/Users/dpete/Documents/programing/E0I/RelicEngine/RelicEngine/Grimphobia/Textures", "*.tsx");

            foreach (string item in files)
            {

                var a = XDocument.Load(item);
                var sheet = new TiledSharp.TmxTileset(a.Document, "path");

                string newpath = item.Replace("tsx", "rsf");

                StreamWriter writer = new StreamWriter(newpath);
                writer.WriteLine("rsf-2.0.0");
                writer.WriteLine(sheet.Name);
                writer.WriteLine(sheet.Image.Source.Replace("path\\", ""));
                writer.WriteLine(sheet.FirstGid);
                writer.WriteLine(sheet.Columns + "," + sheet.TileCount / sheet.Columns);
                writer.WriteLine(sheet.TileWidth + "," + sheet.TileHeight);

                foreach (var tile in sheet.Tiles)
                {
                    if (tile.Value.AnimationFrames.Count > 0)
                    {
                        writer.Write("/A-" + tile.Value.Id + "-" + tile.Value.AnimationFrames.Count);

                        for (int i = 0; i < tile.Value.AnimationFrames.Count; i++)
                        {
                            writer.Write("," + tile.Value.AnimationFrames[i].Id + ":" + tile.Value.AnimationFrames[i].Duration);
                        }
                        writer.WriteLine();
                    }
                }

                // /C-211-3,10:10,20.20,30:30
                //Relative to the top left corner of the tile
                foreach (var tile in sheet.Tiles)
                {
                    if (tile.Value.ObjectGroups.Count > 0)
                    {
                        writer.Write("/C-");
                        writer.Write(tile.Key + "-");
                        var obj = tile.Value.ObjectGroups[0].Objects[0];
                        if (obj.Points != null)
                        {
                            writer.Write(obj.Points.Count + ",");
                            foreach (var point in obj.Points)
                            {
                                writer.Write((point.X +obj.X) + ":" + (point.Y+obj.Y)+",");
                            }
                        }
                        else
                        {
                            writer.Write(4 + ",");
                            writer.Write(obj.X +":"+obj.Y+",");
                            writer.Write(obj.X + ":" + (obj.Y+obj.Height) + ",");
                            writer.Write((obj.X + obj.Width) + ":" + (obj.Y + obj.Height) + ",");
                            writer.Write((obj.X + obj.Width) + ":" + obj.Y + ",");

                        }
                        
                        
                        writer.WriteLine();
                    }
                }

                writer.Close();

            }
        }
    }
}
